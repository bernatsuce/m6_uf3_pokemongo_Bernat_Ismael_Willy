from mongoengine.queryset.transform import update
import pymongo
from pymongo import MongoClient
from pprint import pprint
import mongoengine
import random
from pokemonClass import Team
from datetime import date
from mongoengine.connection import connect
from mongoengine.fields import DateTimeField
from pokemonClass import *
from mongoengine import *
from mongoengine import Document

#Conexiones#
uri = "mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.2.2"
client = MongoClient(uri)

db = client["project"]
pokemons = db["pokemon"]
equipPokemon = db["team"]

#Metodos#
#Query para saber info de los pokemons
def queryParaPokemons(inLine):
    if inLine[2] is not None and inLine[2] != "evol":
        result = conseguirNomPokemon(inLine[1], inLine[2])
        if result is not None:
            tuplaresult = (result["name"], result[inLine[2]])
            pprint(tuplaresult)
        else:
            print("Les dades no són valides, torna a probar amb el nom d'un Pokemon o del Numero del Pokemon a la Pokedex")
    elif inLine[2] == "evol":
        result1 = conseguirNomPokemon(inLine[1], "prev_evolution")
        result2 = conseguirNomPokemon(inLine[1], "next_evolution")

        if "prev_evolution" not in result1 and "next_evolution" in result2:
            tuplaresult = (result2["name"], result2["next_evolution"])
            pprint(tuplaresult)

        elif "next_evolution" not in result2 and "prev_evolution" in result1:
            tuplaresult = (result1["prev_evolution"], result1["name"])
            pprint(tuplaresult)

        elif "prev_evolution" in result1 and "next_evolution" in result2:
            tuplaresult = (result1["prev_evolution"], result2["name"], result2["next_evolution"])
            pprint(tuplaresult)

        elif "prev_evolution" not in result1 and "next_evolution" not in result2:
            tuplaresult = (result2["name"])
            pprint(tuplaresult)
        else:
            print("Les dades no són valides, torna a probar amb el nom d'un Pokemon o del Numero del Pokemon a la Pokedex")
    else:
        print("Les dades no són valides, torna a probar amb el nom d'un Pokemon o del Numero del Pokemon a la Pokedex")


#Buscar pokemon por nombre o numero
def conseguirNomPokemon(name, field):
    result = None
    if field is not None:
        if name.isdigit():
            result = pokemons.find_one({"num": name.zfill(3)}, {"_id": 0, "name":1, field:1})
        elif name.isalpha():
            result = pokemons.find_one({"name": name.capitalize()}, {"_id": 0, "name": 1, field:1})
        else:
            print("error")
    else:
        if name.isdigit():
            result = pokemons.find_one({"num": name.zfill(3)}, {"_id":0, "name":1})
        elif name.isalpha():
            result = pokemons.find_one({"name": name.capitalize()}, {"_id":0, "name":1})
        else:
            print("error")

    return result

#Capturar Pokemons
def capturarPokemon(NomPokemon):
    listaMovCharged = ChargedMove.objects()
    listaMovFast = FastMove.objects()
    pokemonCapturat = Pokemon.objects(name = NomPokemon).first()
    elMeuEquip = Team.objects()
    if pokemonCapturat is None:
        print("El pokemon no existeix")
        return
    elMeuEquip.num = pokemonCapturat.num
    elMeuEquip.name = pokemonCapturat.name
    elMeuEquip.ptype = pokemonCapturat.ptype
    elMeuEquip.HPmax = random.uniform(200.0, 1000.0)
    elMeuEquip.HP = elMeuEquip.HPmax
    elMeuEquip.atk = random.randrange(10, 51, 1)
    elMeuEquip.deff = random.randrange(10, 51, 1)
    elMeuEquip.CP = (elMeuEquip.atk+elMeuEquip.deff+elMeuEquip.HPmax)
    elMeuEquip.energy = 0.0
    fastM = listaMovFast[random.randrange(listaMovFast.count())]
    chargedM = listaMovCharged[random.randrange(listaMovCharged.count())]
    elMeuEquip.moves = movements
    elMeuEquip.candy_count = pokemonCapturat.candy_count
    elMeuEquip.current_candy = 0
    elMeuEquip.weaknesses = pokemonCapturat.weaknesses
    elMeuEquip.save()
    print("Has añadido a "+pokemonCapturat.name)

#Eliminar Pokemons del teu equip
def deleteDePokemons(NomPokemon):
    pokemon = None
    Nom = conseguirNomPokemon(NomPokemon, None)
    try:
        print("Borrar Pokemon: ")
        if NomPokemon.isdigit():
            pokeBorradoNum = equipPokemon.delete_one({"num": NomPokemon.zfill(3)})
        else:
            pokeBorradoNum = equipPokemon.delete_one({"name": NomPokemon.capitalize()})
        CuantsPokemonsTinc = contadorPokemons()
        print(Nom["name"]+" alliberat. Nombre de Pokémons: ",CuantsPokemonsTinc)
    except:
        print("El pokemon no existeix")

#Contador de pokemons en el equipo
def contadorPokemons():
    contador = equipPokemon.count_documents({"_cls" : "Team"})
    return contador

#Update de els carameloRaro, amb evolució
def darCarameloRaro(NomPokemon):
    for pokemons in equipPokemon:
        if pokemons.name == NomPokemon
            pokemons.current_candy += 1
            print("Caramel donat a " + NomPokemon + ". Caramels " + pokemons.current_candy)
            if pokemons.current_candy >= pokemons.candy_count
                PokemonEvoluciona(pokemons.name)

def PokemonEvoluciona(NomPokemon):
    for pokemons in equipPokemon:
        #Para saber si tiene siguiente evolucion
        result = conseguirNomPokemon(NomPokemon, "next_evolution")
        if pokemons.name == NomPokemon and "next_evolution" in result
            namePoke = conseguirNomPokemon(NumEvolucioPokedex, "name")
            numPoke = conseguirNomPokemon(NumEvolucioPokedex, "num")
            candy_countPoke = conseguirNomPokemon(NumEvolucioPokedex, "candy_count")
            NumEvolucioPokedex = pokemons.num + 1
            pokemons.CP += 100
            pokemons.name = namePoke["name"]
            pokemons.num =  numPoke["num"]
            pokemons.candy_count = candy_countPoke["candy_count"]
            pokemons.current_candy = 0
