from http import client
import pymongo
from pymongo import MongoClient
from pprint import pprint


client = MongoClient()
pprint("la base de datos es ")
pprint(client.list_database_names())

#Cliente va a ser diccionario
db = client["project"]
pprint("la db es ")
pprint(db)
#cada db va a ser tambien un diccionario
coll = db["pokemon"]
pprint("la coleccion es ")
pprint(coll)


#coll tiene un monton de metodos
doc = coll.find_one()
pprint("el primer documento es ")
pprint(doc)
#Acceso a doc del diccionario
#nombre
pprint(doc["name"])
#Nombre de su primer amigo
pprint(doc["pokemon"][0]["name"])

