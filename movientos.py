from mongoengine.connection import connect
from classes import *
import mongoengine
from mongoengine import *
from mongoengine import Document
from pprint import pprint

uri = "mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.2.2"
client = connect(host=uri)

#Movimientos#
steelWing = FastMove(name='Steel Wing', pwr=11, moveType="Steel")
steelWing.energyGain = 6
steelWing.save()

dragonTail = FastMove(name='Dragon Tail', pwr=15, moveType="Dragon")
dragonTail.energyGain = 9
dragonTail.save()

ironTail = FastMove(name='Iron Tail', pwr=15, moveType="Steel")
ironTail.energyGain = 7
ironTail.save()

rockThrow = FastMove(name='Rock Throw', pwr=12, moveType="Rock")
rockThrow.energyGain = 7
rockThrow.save()

counter = FastMove(name='Counter', pwr=12, moveType="Fighting")
counter.energyGain = 8
counter.save()

waterfall = FastMove(name='Waterfall', pwr=16, moveType="Water")
waterfall.energyGain = 8
waterfall.save()

thunder = ChargedMove(name='Thunder', pwr=100, moveType="Electric")
thunder.energyCost = 100
thunder.save()

dracoMeteor = ChargedMove(name='Draco Meteor', pwr=150, moveType="Dragon")
dracoMeteor.energyCost = 100
dracoMeteor.save()

megahorn = ChargedMove(name='Megahorn', pwr=90, moveType="Bug")
megahorn.energyCost = 100
megahorn.save()

huracan = ChargedMove(name='Huracan', pwr=110, moveType="Flying")
huracan.energyCost = 100
huracan.save()

overheat = ChargedMove(name='Overheat', pwr=160, moveType="Fire")
overheat.energyCost = 100
overheat.save()

focusBlast = ChargedMove(name='Focus Blast', pwr=140, moveType="Fighting")
focusBlast.energyCost = 100
focusBlast.save()