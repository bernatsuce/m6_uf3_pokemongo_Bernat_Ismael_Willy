import mongoengine as me
from mongoengine import Document
from mongoengine.document import EmbeddedDocument
from mongoengine.fields import *
from datetime import datetime

#Tipos

Types = ("Bug", "Dark", "Dragon", "Electric", "Fairy", "Fighting", "Fire", "Flying", "Ghost",
         "Grass", "Ground", "Ice", "Normal", "Poison", "Psychic", "Rock", "Steel", "Water")

#Movimientos

class Move(Document):
    meta = {'allow_inheritance': True}
    name = StringField(required=True)
    pwr = IntField(required=True, min_value=0, max_value=180)
    moveType = StringField(choices=Types, required=True)


class FastMove(Move):
    moveVelType = StringField("Fast", required=True)
    energyGain = IntField(required=True, min_value=0, max_value=20)


class ChargedMove(Move):
    moveVelType = StringField("Charged", required=True)
    energyCost = IntField(required=True, min_value=33, max_value=100)

#Equipo
class Team (Document):
    meta = {'allow_inheritance': True}
    num = StringField()
    name = StringField()
    ptype = ListField(db_field="type", choices=Types)
    catch_date = DateTimeField(default=datetime.now())
    CP = FloatField(required=True)
    HPmax = FloatField(required=True, min_value=200.0, max_value=1000.0)
    HP = FloatField(required=True)
    atk = IntField(required=True, min_value=10, max_value=50)
    deff = IntField(db_fild="def", required=True, min_value=10, max_value=50)
    energy = IntField(required=True, min_value=0, max_value=100)
    moves = EmbeddedDocumentField(move)
    current_candy = IntField(required=True)
    candy_count = IntField(required=True, min_value=0, max_value=500)
    weaknesses = ListField(choices=Types)

#Pokemons
class Evolution(EmbeddedDocument):
    num = StringField()
    name = StringField()


class Pokemon(Document):
    pid = IntField(db_field="id", required=True)
    num = StringField(required=True)
    name = StringField(required=True)
    img = StringField(required=True)
    ptype = ListField(db_field="type", choices=Types, required=True)
    height = StringField(required=True)
    weight = StringField(required=True)
    candy = StringField(required=True)
    candy_count = IntField(required=True, min_value=0, max_value=500)
    egg = StringField(required=True)
    spawn_chance = FloatField(required=True)
    avg_spawns = FloatField(required=True)
    spawn_time = StringField(required=True)
    multipliers = ListField(FloatField(required=True))
    weaknesses = ListField(choices=Types, required=True)
    next_evolution = ListField(EmbeddedDocumentField(Evolution))
    prev_evolution = ListField(EmbeddedDocumentField(Evolution))
